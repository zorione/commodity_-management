package com.zorione.CommodityManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommodityManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommodityManagementApplication.class,args);
    }

}
