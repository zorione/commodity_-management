package com.zorione.CommodityManagement.model;


import java.sql.Timestamp;
import java.util.Arrays;

public class Product {
    private Integer id;
    private String productName; //'商品名称'
    private String weight; //'商品重量'
    private Integer productType; //'商品类型'
    private String sellingPrice; //'销售价格'
    private String costPrice; //'成本价'
    private String estimatedShippingCost; //'预估快递费用'
    private Integer stockQuantity; //'库存'
    private Integer isShelves; //'是否需要下架 1 需要 2 不需要'
    private String imagePath; //'图片路径'
    private byte[] imageBinary; //'二进制图片'
    private String typeName; //商品类型
    private String description; //'描述'
    private Timestamp createdTime;
    private Timestamp updatedTime;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", weight='" + weight + '\'' +
                ", productType=" + productType +
                ", sellingPrice='" + sellingPrice + '\'' +
                ", costPrice='" + costPrice + '\'' +
                ", estimatedShippingCost='" + estimatedShippingCost + '\'' +
                ", stockQuantity=" + stockQuantity +
                ", isShelves=" + isShelves +
                ", imagePath='" + imagePath + '\'' +
                ", imageBinary=" + Arrays.toString(imageBinary) +
                ", typeName='" + typeName + '\'' +
                ", description='" + description + '\'' +
                ", createdTime=" + createdTime +
                ", updatedTime=" + updatedTime +
                '}';
    }



    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(String sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getEstimatedShippingCost() {
        return estimatedShippingCost;
    }

    public void setEstimatedShippingCost(String estimatedShippingCost) {
        this.estimatedShippingCost = estimatedShippingCost;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public Integer getIsShelves() {
        return isShelves;
    }

    public void setIsShelves(Integer isShelves) {
        this.isShelves = isShelves;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public byte[] getImageBinary() {
        return imageBinary;
    }

    public void setImageBinary(byte[] imageBinary) {
        this.imageBinary = imageBinary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }


}
