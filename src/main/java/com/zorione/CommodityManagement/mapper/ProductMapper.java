package com.zorione.CommodityManagement.mapper;

import com.zorione.CommodityManagement.model.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ProductMapper {
    /**
     * 查询所有商品信息
     * @return
     * @param productNumber
     * @param productName
     * @param productType
     */
    List<Product> selectProductList(String productNumber, String productName, String productType);

}
