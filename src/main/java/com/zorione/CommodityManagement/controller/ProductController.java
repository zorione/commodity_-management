package com.zorione.CommodityManagement.controller;

import com.zorione.CommodityManagement.model.Product;
import com.zorione.CommodityManagement.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("/allProducts")
    @ResponseBody
    public List<Product> ListProducts(@RequestParam String productNumber,@RequestParam String productName,@RequestParam String productType){
        Map map = new HashMap();
        map.put("productNumber",productNumber);
        map.put("productName",productName);
        map.put("productType",productType);
        List<Product> allProducts = productService.getAllProducts(map);
        return allProducts;
    }


}
