package com.zorione.CommodityManagement.controller;

import com.zorione.CommodityManagement.model.User;
import com.zorione.CommodityManagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String redirectToLogin() {
        // 默认重定向到登录页面
        return "login";
    }

    @PostMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password, Model model) {
        User user = userService.findByUsername(username);
        if (user != null && user.getPassword().equals(password)) {
            // 登录成功，重定向到商品页面
            return "Product";
        } else {
            // 登录失败，返回登录页面并添加错误信息到模型中
            model.addAttribute("error", "Invalid username or password");
            return "login";
        }
    }

}
