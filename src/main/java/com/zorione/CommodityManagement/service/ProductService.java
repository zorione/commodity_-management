package com.zorione.CommodityManagement.service;

import com.zorione.CommodityManagement.model.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

    List<Product> getAllProducts(Map map);

}
