package com.zorione.CommodityManagement.service;

import com.zorione.CommodityManagement.model.User;

public interface UserService {

    User findByUsername(String username);

}
