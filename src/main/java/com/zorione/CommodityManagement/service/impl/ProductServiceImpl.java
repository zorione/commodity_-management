package com.zorione.CommodityManagement.service.impl;

import com.zorione.CommodityManagement.mapper.ProductMapper;
import com.zorione.CommodityManagement.model.Product;
import com.zorione.CommodityManagement.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> getAllProducts(Map map) {
        String productNumber = map.get("productNumber").toString();
        String productName = map.get("productName").toString();
        String productType = map.get("productType").toString();
        return productMapper.selectProductList(productNumber,productName,productType);
    }

}
