package com.zorione.CommodityManagement.service.impl;

import com.zorione.CommodityManagement.mapper.UserMapper;
import com.zorione.CommodityManagement.model.User;
import com.zorione.CommodityManagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }


}
